## National Bank of Poland Currencies Rates

NBP publish currencies exchange rates in text files according to following instruction 
http://www.nbp.pl/homen.aspx?f=/kursy/instrukcja_pobierania_kursow_walut_en.html

NBPRates application shows currency exchange rate average and standard deviation from the given period.

## Usage
##### Command
```
java -jar NBPRates-1.0.jar EUR 2017-04-19 2017-04-20
```

##### Example Output
```
           Average of EUR rates from 2017-04-19 to 2017-04-20 = 4.1972
Standard Deviation of EUR rates from 2017-04-19 to 2017-04-20 = 0.0066
```

## Building
Building requires:
 - Apache Maven ≥ 3
 - Java Development Kit ≥ 1.8
 
```
mvn clean install
```



