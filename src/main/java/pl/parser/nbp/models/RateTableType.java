package pl.parser.nbp.models;

import java.util.Arrays;

/**
 * @see 'http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html'
 */
public enum RateTableType {

    FOREIGN_CURRENCIES("a"),
    UNEXCHANGEABLE_CURRENCIES("b"),
    BUY_AND_SELL("c"),
    BILLING_UNITS("h");

    private final String filePrefix;

    RateTableType(String filePrefix) {
        this.filePrefix = filePrefix;
    }

    public String getFilePrefix() {
        return filePrefix;
    }

    public static RateTableType ofPrefix(String prefix) {
        return Arrays.stream(values())
                .filter(v -> v.getFilePrefix().equals(prefix))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("RateTableType with prefix " + prefix + " does not exist"));
    }
}
