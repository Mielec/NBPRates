package pl.parser.nbp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rate {
    private String currencyName;
    private BigInteger ratio;
    private String currency;
    private BigDecimal buyRate;
    private BigDecimal sellRate;
}
