package pl.parser.nbp.models;

import com.google.common.base.Preconditions;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
public class RateFileName {
    private static final String DELIMITER = "z";
    private static final String EXTENSION = ".xml";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.BASIC_ISO_DATE;

    private final RateTableType type;
    private final Integer number;
    private final LocalDate date;

    private RateFileName(RateTableType type, Integer number, LocalDate date) {
        this.type = type;
        this.number = number;
        this.date = date;
    }

    public static RateFileName ofFilename(String filename) {
        if (filename.endsWith(EXTENSION)) {
            filename = filename.replace(EXTENSION, "");
        }

        Preconditions.checkNotNull(filename);
        Preconditions.checkArgument(filename.length() == 11, "Filename %s has incorrect length", filename);

        String type = filename.substring(0, 1);
        String number = filename.substring(1, 4);
        String date = "20" + filename.substring(5, 11);

        return new RateFileName(
                RateTableType.ofPrefix(type),
                Integer.parseInt(number),
                LocalDate.parse(date, DATE_TIME_FORMATTER)
        );
    }

    public String getFileName() {
        return type.getFilePrefix() +
                String.format("%03d", getNumber()) +
                DELIMITER +
                getDate().format(DATE_TIME_FORMATTER).substring(2) +
                EXTENSION;
    }
}
