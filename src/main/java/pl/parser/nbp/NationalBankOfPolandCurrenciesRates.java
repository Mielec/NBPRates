package pl.parser.nbp;

import com.google.inject.Guice;
import org.xml.sax.SAXException;
import pl.parser.nbp.services.RatesService;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NationalBankOfPolandCurrenciesRates {

    private NationalBankOfPolandCurrenciesRates() {
    }

    public static void main(String[] args) {

        String currency = args[0];
        LocalDate from = LocalDate.parse(args[1], DateTimeFormatter.ISO_DATE);
        LocalDate to = LocalDate.parse(args[2], DateTimeFormatter.ISO_DATE);

        RatesService ratesService = Guice.createInjector(new Configuration()).getInstance(RatesService.class);

        BigDecimal average;
        BigDecimal standardDeviation;
        try {
            average = ratesService.countBuyRatesAverage(currency, from, to);
            standardDeviation = ratesService.countSellRatesStandardDeviation(currency, from, to);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Connection problem");
            return;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            System.err.println("Parsing XML problem");
            return;
        } catch (SAXException e) {
            e.printStackTrace();
            System.err.println("Incorrect XML syntax");
            return;
        }
        System.out.println(String.format("           Average of %s rates from %s to %s = %s", currency, from, to, average));
        System.out.println(String.format("Standard Deviation of %s rates from %s to %s = %s", currency, from, to, standardDeviation));
    }

}
