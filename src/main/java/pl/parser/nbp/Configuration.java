package pl.parser.nbp;

import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.name.Names;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import pl.parser.nbp.repositories.NBPRatesFileNamesRepository;
import pl.parser.nbp.repositories.NBPRatesRepository;
import pl.parser.nbp.repositories.RateFileNamesRepository;
import pl.parser.nbp.repositories.RatesRepository;
import pl.parser.nbp.services.NBPRatesService;
import pl.parser.nbp.services.RatesService;

import java.io.File;
import java.time.LocalDate;

public class Configuration extends AbstractModule {

    public static final String BASE_URL_NAME = "baseUrl";

    @Override
    protected void configure() {
        bind(RateFileNamesRepository.class).to(NBPRatesFileNamesRepository.class);
        bind(RatesRepository.class).to(NBPRatesRepository.class);
        bind(RatesService.class).to(NBPRatesService.class);
        bind(LocalDate.class).toProvider(CurrentDateProvider.class);
        bind(String.class).annotatedWith(Names.named(BASE_URL_NAME)).toInstance("http://www.nbp.pl/kursy/xml");
    }

    @Provides
    OkHttpClient okHttpClient() {
        Cache cache = new Cache(new File("cache"), 10 * 1024 * 1024);
        return new OkHttpClient.Builder()
                .cache(cache)
                .build();
    }

    private static final class CurrentDateProvider implements Provider<LocalDate> {
        @Override
        public LocalDate get() {
            return LocalDate.now();
        }
    }


}
