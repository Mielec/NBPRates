package pl.parser.nbp.repositories;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import pl.parser.nbp.Configuration;
import pl.parser.nbp.models.RateFileName;

import java.io.*;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class NBPRatesFileNamesRepository implements RateFileNamesRepository {
    private static final String RATES_LIST_FILE_NAME = "dir";
    private static final String RATES_LIST_FILE_EXTENSION = ".txt";

    private final OkHttpClient client;
    private final LocalDate today;
    private final String baseAddress;

    @Inject
    public NBPRatesFileNamesRepository(OkHttpClient client, @Named(Configuration.BASE_URL_NAME) String baseAddress, LocalDate today) {
        this.client = client;
        this.today = today;
        this.baseAddress = baseAddress;
    }

    public List<RateFileName> getRateFileNamesByYear(Year year) throws IOException {
        String url = baseAddress + "/" + prepareYearFilename(year);

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        BOMInputStream bomInputStream = new BOMInputStream(response.body().byteStream());
        String body = IOUtils.toString(bomInputStream, Charsets.UTF_8).trim();

        if (body.isEmpty()) {
            return new ArrayList<>();
        }

        return Arrays.stream(body.split("\\r?\\n"))
                .map(String::trim)
                .map(RateFileName::ofFilename)
                .collect(Collectors.toList());
    }

    public List<RateFileName> getRateFileNamesForPeriod(LocalDate from, LocalDate to) throws IOException {
        List<RateFileName> fileNames = new ArrayList<>();
        for (int year = from.getYear(); year <= to.getYear(); year++) {
            fileNames.addAll(getRateFileNamesByYear(Year.of(year)));
        }

        fileNames = fileNames.stream()
                .filter(n -> isDateInPeriod(n.getDate(), from, to))
                .collect(Collectors.toList());

        return fileNames;
    }

    private String prepareYearFilename(Year year) {
        String yearRatesFileName = RATES_LIST_FILE_NAME;
        if (today.getYear() != year.getValue()) {
            yearRatesFileName += Integer.toString(year.getValue());
        }
        yearRatesFileName += RATES_LIST_FILE_EXTENSION;
        return yearRatesFileName;
    }

    private boolean isDateInPeriod(LocalDate date, LocalDate from, LocalDate to) {
        return (!date.isBefore(from)) && (!date.isAfter(to));
    }


}
