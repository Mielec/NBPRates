package pl.parser.nbp.repositories;

import pl.parser.nbp.models.RateFileName;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Year;
import java.util.List;

public interface RateFileNamesRepository {

    List<RateFileName> getRateFileNamesByYear(Year year) throws IOException;

    List<RateFileName> getRateFileNamesForPeriod(LocalDate from, LocalDate to) throws IOException;
}
