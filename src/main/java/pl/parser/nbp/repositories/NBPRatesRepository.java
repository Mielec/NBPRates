package pl.parser.nbp.repositories;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.xml.sax.SAXException;
import pl.parser.nbp.Configuration;
import pl.parser.nbp.models.Rate;
import pl.parser.nbp.sax.DailyRateHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class NBPRatesRepository implements RatesRepository {

    private final OkHttpClient client;
    private final String baseAddress;

    @Inject
    public NBPRatesRepository(OkHttpClient client, @Named(Configuration.BASE_URL_NAME) String baseAddress) {
        this.client = client;
        this.baseAddress = baseAddress;
    }

    public List<Rate> getRates(String fileName) throws IOException, SAXException {
        String url = baseAddress + "/" + fileName;

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        SAXParser parser = null;
        try {
            parser = SAXParserFactory.newInstance().newSAXParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new IllegalStateException("XML Parser not properly configured.");
        }
        DailyRateHandler dailyRateHandler = new DailyRateHandler();
        parser.parse(response.body().byteStream(), dailyRateHandler);
        return dailyRateHandler.getRates();
    }
}
