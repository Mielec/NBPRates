package pl.parser.nbp.repositories;

import org.xml.sax.SAXException;
import pl.parser.nbp.models.Rate;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public interface RatesRepository {

    List<Rate> getRates(String fileName) throws IOException, SAXException;
}
