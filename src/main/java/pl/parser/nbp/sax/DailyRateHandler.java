package pl.parser.nbp.sax;

import com.google.common.collect.ImmutableMap;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import pl.parser.nbp.models.Rate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class DailyRateHandler extends DefaultHandler {

    private List<Rate> rates = new ArrayList<>();
    private Rate rate;
    private String currentElement;

    private static final String XML_RATE_ELEMENT = "pozycja";
    private static final String XML_CURRENCY_NAME_ELEMENT = "nazwa_waluty";
    private static final String XML_RATIO_ELEMENT = "przelicznik";
    private static final String XML_CURRENCY_ELEMENT = "kod_waluty";
    private static final String XML_BUY_RATE_ELEMENT = "kurs_kupna";
    private static final String XML_SELL_RATE_ELEMENT = "kurs_sprzedazy";

    private final Map<String, Consumer<String>> handlers = ImmutableMap.of(
            XML_CURRENCY_NAME_ELEMENT, s -> rate.setCurrencyName(s),
            XML_RATIO_ELEMENT, s -> rate.setRatio(new BigInteger(s)),
            XML_CURRENCY_ELEMENT, s -> rate.setCurrency(s),
            XML_BUY_RATE_ELEMENT, s -> rate.setBuyRate(new BigDecimal(s.replace(",", "."))),
            XML_SELL_RATE_ELEMENT, s -> rate.setSellRate(new BigDecimal(s.replace(",", ".")))
    );

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals(XML_RATE_ELEMENT)) {
            rate = new Rate();
        }
        currentElement = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase(XML_RATE_ELEMENT)) {
            rates.add(rate);
        }
        currentElement = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (!handlers.containsKey(currentElement)) {
            return;
        }
        handlers.get(currentElement).accept(new String(ch, start, length));
    }

    public List<Rate> getRates() {
        return rates;
    }
}
