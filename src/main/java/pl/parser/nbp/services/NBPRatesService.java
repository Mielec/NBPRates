package pl.parser.nbp.services;

import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import org.xml.sax.SAXException;
import pl.parser.nbp.models.Rate;
import pl.parser.nbp.models.RateFileName;
import pl.parser.nbp.models.RateTableType;
import pl.parser.nbp.repositories.NBPRatesFileNamesRepository;
import pl.parser.nbp.repositories.NBPRatesRepository;
import pl.parser.nbp.repositories.RateFileNamesRepository;
import pl.parser.nbp.repositories.RatesRepository;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NBPRatesService implements RatesService {

    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
    private static final int COUNT_PRECISION = 6;
    private static final int RETURN_PRECISION = 4;

    private final RateFileNamesRepository ratesFileNamesRepository;
    private final RatesRepository NBPRatesRepository;

    @Inject
    public NBPRatesService(RateFileNamesRepository rateFileNamesRepository, RatesRepository ratesRepository) {
        this.ratesFileNamesRepository = rateFileNamesRepository;
        this.NBPRatesRepository = ratesRepository;
    }

    public BigDecimal countBuyRatesAverage(String currency, LocalDate from, LocalDate to) throws IOException, SAXException {
        validateArguments(currency, from, to);

        List<Rate> rates = getRatesByCurrency(currency, from, to);

        BigDecimal sum = rates.stream()
                .map(Rate::getBuyRate)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return sum.divide(new BigDecimal(rates.size()), RETURN_PRECISION, ROUNDING_MODE);

    }

    public BigDecimal countSellRatesStandardDeviation(String currency, LocalDate from, LocalDate to) throws SAXException, IOException {
        validateArguments(currency, from, to);

        List<Rate> rates = getRatesByCurrency(currency, from, to);

        BigDecimal sellRatesSum = rates.stream()
                .map(Rate::getSellRate)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal averageSellRate = sellRatesSum.divide(new BigDecimal(rates.size()), COUNT_PRECISION, ROUNDING_MODE);

        BigDecimal standardDeviation = rates.stream()
                .map(Rate::getSellRate)
                .map(r -> r.subtract(averageSellRate).pow(2))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        standardDeviation = standardDeviation.divide(new BigDecimal(rates.size()), COUNT_PRECISION, ROUNDING_MODE);
        return BigDecimal.valueOf(Math.sqrt(standardDeviation.doubleValue())).setScale(RETURN_PRECISION, ROUNDING_MODE);
    }

    private List<Rate> getRatesByCurrency(String currency, LocalDate from, LocalDate to) throws SAXException, IOException {
        List<RateFileName> fileNames = ratesFileNamesRepository.getRateFileNamesForPeriod(from, to).stream()
                .filter(l -> l.getType() == RateTableType.BUY_AND_SELL)
                .collect(Collectors.toList());

        List<Rate> rates = new ArrayList<>();
        for (RateFileName name : fileNames) {
            rates.addAll(NBPRatesRepository.getRates(name.getFileName()).stream()
                    .filter(r -> r.getCurrency().equals(currency))
                    .collect(Collectors.toList()));
        }
        return rates;
    }

    private void validateArguments(String currency, LocalDate from, LocalDate to) {
        Preconditions.checkArgument(from.isBefore(to), "Begin date (" + from + ") is not before end date (" + to + ")");
        Preconditions.checkArgument(currency.length() == 3, "Currency " + currency + "should have length 3");
    }


}
