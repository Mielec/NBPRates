package pl.parser.nbp.services;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

public interface RatesService {

    BigDecimal countBuyRatesAverage(String currency, LocalDate from, LocalDate to) throws IOException, ParserConfigurationException, SAXException;

    BigDecimal countSellRatesStandardDeviation(String currency, LocalDate from, LocalDate to) throws ParserConfigurationException, SAXException, IOException;
}
