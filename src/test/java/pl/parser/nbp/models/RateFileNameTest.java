package pl.parser.nbp.models;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeParseException;

import static org.assertj.core.api.Assertions.*;

@RunWith(JUnitParamsRunner.class)
public class RateFileNameTest {

    private static final String CORRECT_FILE_NAME = "c026z100208";
    private static final String CORRECT_FILE_NAME_WITH_EXTENSION = "c026z100208.xml";
    private static final RateTableType CORRECT_TYPE = RateTableType.BUY_AND_SELL;
    private static final Integer CORRECT_NUMBER = 26;
    private static final LocalDate CORRECT_DATE = LocalDate.of(2010, Month.FEBRUARY, 8);

    private static final String UNEXISTING_PREFIX_FILE_NAME = "w026z100208";
    private static final String TOO_BIG_FILE_NAME = "c022136z1002082";
    private static final String TOO_SHORT_FILE_NAME = "c100208";
    private static final String INVALID_MONTH_FILE_NAME = "c026z102008";
    private static final String INVALID_DAY_FILE_NAME = "c026z100255";

    @Test
    @Parameters({
            CORRECT_FILE_NAME,
            CORRECT_FILE_NAME_WITH_EXTENSION
    })
    public void ofFilename_correctName_shouldCreateObject(String correctName) {
        //given
        //when
        RateFileName result = RateFileName.ofFilename(correctName);

        //then
        assertThat(result.getType()).isEqualTo(CORRECT_TYPE);
        assertThat(result.getNumber()).isEqualTo(CORRECT_NUMBER);
        assertThat(result.getDate()).isEqualTo(CORRECT_DATE);
    }

    @Test
    @Parameters(value = {
            UNEXISTING_PREFIX_FILE_NAME,
            TOO_BIG_FILE_NAME,
            TOO_SHORT_FILE_NAME,
    })
    public void ofFilename_incorrectName_shouldThrowIllegalArgument(String invalidName) {
        //given
        //when
        Throwable thrown = catchThrowable(() -> RateFileName.ofFilename(invalidName));

        //then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @Parameters(value = {
            INVALID_MONTH_FILE_NAME,
            INVALID_DAY_FILE_NAME
    })
    public void ofFilename_incorrectName_shouldThrowDateTimeParse(String invalidName) {
        //given
        //when
        Throwable thrown = catchThrowable(() -> RateFileName.ofFilename(invalidName));

        //then
        assertThat(thrown).isInstanceOf(DateTimeParseException.class);
    }

    @Test
    public void getFileName_shouldReturnFormattedFileName() {
        //given
        RateFileName rateFileName = RateFileName.ofFilename(CORRECT_FILE_NAME);

        //when
        String result = rateFileName.getFileName();

        //then
        assertThat(result).isEqualTo(CORRECT_FILE_NAME + ".xml");
    }

}