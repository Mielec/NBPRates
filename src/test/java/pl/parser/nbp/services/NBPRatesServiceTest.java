package pl.parser.nbp.services;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.xml.sax.SAXException;
import pl.parser.nbp.models.Rate;
import pl.parser.nbp.models.RateFileName;
import pl.parser.nbp.repositories.RateFileNamesRepository;
import pl.parser.nbp.repositories.RatesRepository;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class NBPRatesServiceTest {

    private RateFileNamesRepository rateFileNamesRepository = mock(RateFileNamesRepository.class);

    private RatesRepository ratesRepository = mock(RatesRepository.class);

    private static final List<RateFileName> SOME_FILE = Arrays.asList(RateFileName.ofFilename("c026z100208"));
    private static final List<Rate> CORRECT_RATES = Arrays.asList(
            new Rate("dolar amerykański", BigInteger.valueOf(1), "USD", new BigDecimal("1.0"), new BigDecimal("5.0")),
            new Rate("dolar amerykański", BigInteger.valueOf(1), "USD", new BigDecimal("2.0"), new BigDecimal("5.2")),
            new Rate("dolar amerykański", BigInteger.valueOf(1), "USD", new BigDecimal("3.0"), new BigDecimal("5.6"))
    );

    private static final List<Rate> CORRECT_RATES_WITH_OTHER_CURRENCIES = Arrays.asList(
            new Rate("dolar amerykański", BigInteger.valueOf(1), "USD", new BigDecimal("1.0"), new BigDecimal("5.0")),
            new Rate("dolar amerykański", BigInteger.valueOf(1), "USD", new BigDecimal("2.0"), new BigDecimal("5.2")),
            new Rate("dolar amerykański", BigInteger.valueOf(1), "USD", new BigDecimal("3.0"), new BigDecimal("5.6")),
            new Rate("euro", BigInteger.valueOf(1), "EUR", new BigDecimal("300.0"), new BigDecimal("525.6")),
            new Rate("euro", BigInteger.valueOf(1), "EUR", new BigDecimal("300.0"), new BigDecimal("123.0"))
    );

    private static final BigDecimal CORRECT_BUY_RATES_AVERAGE = new BigDecimal("2.0").setScale(4);
    private static final BigDecimal CORRECT_SELL_RATES_STANDARD_DEVIATION = new BigDecimal("0.2494").setScale(4);

    private static final BigDecimal CORRECT_BUY_RATES_WITH_OTHER_CURRENCIES_AVERAGE = new BigDecimal("300.0").setScale(4);
    private static final BigDecimal CORRECT_BUY_RATES_WITH_OTHER_CURRENCIES_STANDARD_DEVIATION = new BigDecimal("201.3").setScale(4);


    @Test
    public void countBuyRatesAverage_correctData_shouldCount() throws IOException, SAXException {
        //given
        NBPRatesService NBPRatesService = new NBPRatesService(rateFileNamesRepository, ratesRepository);
        when(rateFileNamesRepository.getRateFileNamesForPeriod(anyObject(), anyObject())).thenReturn(SOME_FILE);
        when(ratesRepository.getRates(anyString())).thenReturn(CORRECT_RATES);

        BigDecimal result = NBPRatesService.countBuyRatesAverage("USD", LocalDate.of(2015, Month.APRIL, 22), LocalDate.of(2015, Month.SEPTEMBER, 22));

        //then
        Assertions.assertThat(result).isEqualTo(CORRECT_BUY_RATES_AVERAGE);
    }

    @Test
    public void countBuyRatesAverage_fewCurrenciesGiven_shouldCountOnlyRequested() throws IOException, SAXException {
        //given
        NBPRatesService NBPRatesService = new NBPRatesService(rateFileNamesRepository, ratesRepository);
        when(rateFileNamesRepository.getRateFileNamesForPeriod(anyObject(), anyObject())).thenReturn(SOME_FILE);
        when(ratesRepository.getRates(anyString())).thenReturn(CORRECT_RATES_WITH_OTHER_CURRENCIES);

        BigDecimal result = NBPRatesService.countBuyRatesAverage("EUR", LocalDate.of(2015, Month.APRIL, 22), LocalDate.of(2015, Month.SEPTEMBER, 22));

        //then
        Assertions.assertThat(result).isEqualTo(CORRECT_BUY_RATES_WITH_OTHER_CURRENCIES_AVERAGE);
    }

    @Test
    public void countSellRatesStandardDeviation_correctData_shouldCount() throws IOException, SAXException {
        //given
        NBPRatesService NBPRatesService = new NBPRatesService(rateFileNamesRepository, ratesRepository);
        when(rateFileNamesRepository.getRateFileNamesForPeriod(anyObject(), anyObject())).thenReturn(SOME_FILE);
        when(ratesRepository.getRates(anyString())).thenReturn(CORRECT_RATES);

        BigDecimal result = NBPRatesService.countSellRatesStandardDeviation("USD", LocalDate.of(2015, Month.APRIL, 22), LocalDate.of(2015, Month.SEPTEMBER, 22));

        //then
        Assertions.assertThat(result).isEqualTo(CORRECT_SELL_RATES_STANDARD_DEVIATION);
    }

    @Test
    public void countSellRatesStandardDeviation_fewCurrenciesGiven_shouldCountOnlyRequested() throws IOException, SAXException {
        //given
        NBPRatesService NBPRatesService = new NBPRatesService(rateFileNamesRepository, ratesRepository);
        when(rateFileNamesRepository.getRateFileNamesForPeriod(anyObject(), anyObject())).thenReturn(SOME_FILE);
        when(ratesRepository.getRates(anyString())).thenReturn(CORRECT_RATES_WITH_OTHER_CURRENCIES);

        BigDecimal result = NBPRatesService.countSellRatesStandardDeviation("EUR", LocalDate.of(2015, Month.APRIL, 22), LocalDate.of(2015, Month.SEPTEMBER, 22));

        //then
        Assertions.assertThat(result).isEqualTo(CORRECT_BUY_RATES_WITH_OTHER_CURRENCIES_STANDARD_DEVIATION);
    }

}