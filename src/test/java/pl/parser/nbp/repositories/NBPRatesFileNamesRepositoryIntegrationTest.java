package pl.parser.nbp.repositories;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import pl.parser.nbp.models.RateFileName;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class NBPRatesFileNamesRepositoryIntegrationTest {
    private static final LocalDate TODAY = LocalDate.of(2017, Month.APRIL, 22);

    private MockWebServer server = null;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @After
    public void tearDown() throws IOException {
        if (server == null) {
            return;
        }
        server.shutdown();
    }

    @Test
    @Parameters({
            "file-list-responses/2017CurrencyRatesFileList.txt, 250",
            "file-list-responses/WhitespacePrefixedCurrencyRatesFileList.txt, 250",
            "file-list-responses/BOMPrefixedCurrencyRatesFileList.txt, 250",
            "file-list-responses/EmptyCurrencyRatesFileList.txt, 0"
    })
    public void getRatingsFileNames_connectionIsCorrect_shouldReturnListOfFiles(String bodyResource, int expectedSize) throws IOException {
        //given
        HttpUrl httpUrl = prepareServerReturningResource(bodyResource, "/test");
        NBPRatesFileNamesRepository repository = new NBPRatesFileNamesRepository(new OkHttpClient(), httpUrl.toString(), TODAY);

        //when
        List<RateFileName> result = repository.getRateFileNamesByYear(Year.of(2016));

        //then
        assertThat(result).hasSize(expectedSize);
    }

    @Test
    public void getRatingsFileNames_currentYear_shouldRequestToDirWithoutYear() throws IOException, InterruptedException {
        //given
        HttpUrl url = prepareServerReturningResource("file-list-responses/2017CurrencyRatesFileList.txt", "/test");
        NBPRatesFileNamesRepository NBPRatesFileNamesRepository = new NBPRatesFileNamesRepository(new OkHttpClient(), url.toString(), TODAY);

        //when
        NBPRatesFileNamesRepository.getRateFileNamesByYear(Year.of(TODAY.getYear()));

        //then
        RecordedRequest request = server.takeRequest();
        assertThat(request.getPath()).isEqualTo("/test/dir.txt");
    }

    @Test
    public void getRatingsFileNames_notCurrentYear_shouldRequestToDirWithYear() throws IOException, InterruptedException {
        //given
        HttpUrl url = prepareServerReturningResource("file-list-responses/2017CurrencyRatesFileList.txt", "/test");
        NBPRatesFileNamesRepository NBPRatesFileNamesRepository = new NBPRatesFileNamesRepository(new OkHttpClient(), url.toString(), TODAY);
        Year lastYear = Year.of(TODAY.minusYears(1).getYear());

        //when
        NBPRatesFileNamesRepository.getRateFileNamesByYear(lastYear);

        //then
        RecordedRequest request = server.takeRequest();
        String withYearPath = String.format("/test/dir%s.txt", lastYear);
        assertThat(request.getPath()).isEqualTo(withYearPath);
    }

    @Test
    public void getRatingsFileNames_connectionIsIncorrect_shouldThrowIOException() throws IOException {
        //given
        NBPRatesFileNamesRepository NBPRatesFileNamesRepository = new NBPRatesFileNamesRepository(new OkHttpClient(), "http://www.localhost:23254", TODAY);

        thrown.expect(IllegalArgumentException.class);

        //when
        NBPRatesFileNamesRepository.getRateFileNamesByYear(Year.of(2016));
    }

    private HttpUrl prepareServerReturningResource(String resourceName, String path) throws IOException {
        server = new MockWebServer();
        String body = Resources.toString(Resources.getResource(resourceName), Charsets.UTF_8);
        server.enqueue(new MockResponse().setBody(body));
        server.start();
        return server.url(path);

    }
}