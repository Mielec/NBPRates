package pl.parser.nbp.repositories;


import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;
import pl.parser.nbp.models.Rate;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class NBPRatesRepositoryIntegrationTest {

    private static final String EXAMPLE_XML =
            "   <tabela_kursow typ=\"C\">\n" +
                    "   <numer_tabeli>73/C/NBP/2007</numer_tabeli>\n" +
                    "   <data_notowania>2007-04-12</data_notowania>\n" +
                    "   <data_publikacji>2007-04-13</data_publikacji>\n" +
                    "   <pozycja>\n" +
                    "      <nazwa_waluty>dolar amerykański</nazwa_waluty>\n" +
                    "      <przelicznik>1</przelicznik>\n" +
                    "      <kod_waluty>USD</kod_waluty>\n" +
                    "      <kurs_kupna>2,8210</kurs_kupna>\n" +
                    "      <kurs_sprzedazy>2,8780</kurs_sprzedazy>\n" +
                    "   </pozycja>\n" +
                    "   <pozycja>\n" +
                    "      <nazwa_waluty>dolar australijski</nazwa_waluty>\n" +
                    "      <przelicznik>1</przelicznik>\n" +
                    "      <kod_waluty>AUD</kod_waluty>\n" +
                    "      <kurs_kupna>2,3292</kurs_kupna>\n" +
                    "      <kurs_sprzedazy>2,3762</kurs_sprzedazy>\n" +
                    "   </pozycja>\n" +
                    "</tabela_kursow>";

    private static final Rate FIRST_RATE = new Rate("dolar amerykański", BigInteger.valueOf(1), "USD", new BigDecimal("2.8210"), new BigDecimal("2.8780"));
    private static final Rate SECOND_RATE = new Rate("dolar australijski", BigInteger.valueOf(1), "AUD", new BigDecimal("2.3292"), new BigDecimal("2.3762"));

    private MockWebServer server;

    @Before
    public void setUp() throws IOException {
        server = new MockWebServer();
        server.enqueue(new MockResponse().setBody(EXAMPLE_XML));
        server.start();
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }

    @Test
    public void getRates_correctXml_shouldParseXml() throws IOException, SAXException {
        //given
        NBPRatesRepository nbpRatesRepository = new NBPRatesRepository(new OkHttpClient(), server.url("/test").toString());

        //when
        List<Rate> result = nbpRatesRepository.getRates("someFile");

        //then
        Assertions.assertThat(result).contains(FIRST_RATE).contains(SECOND_RATE);
    }

}